﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardLibrary;

namespace Enlightenment_Business {
    public class AI : Rules {

        //AI Contorl Objects
        private Card[,] hands;
        private int[] levels;

        //AI Details
        private int cardsPerHand = 0;
        private int numberOfPlayers = 0;


        // =====================================
        //Constructs ---------------------------
        // =====================================

        /// <summary>
        /// Makes A Set of AI
        /// Players
        /// </summary>
        /// <param name="numPlayers">Integer - How many players in game</param>
        /// <param name="handSize">Integer - Number of cards in a dealt hand</param>
        public AI(int numPlayers, int handSize) {
            this.hands = new Card[numPlayers-1, handSize];
            this.levels = new int[numPlayers-1];
            this.cardsPerHand = handSize;
            this.numberOfPlayers = numPlayers-1;
        
        } //end construct

        // =====================================



        // =====================================
        //Fields -------------------------------
        // =====================================

        /// <summary>
        /// Gets or Sets The Cards Within 
        /// The Ai Players Hands
        /// </summary>
        public Card[,] AIHands { get { return this.hands; } 
                                 set { this.hands=value; } 
                                }


        /// <summary>
        /// Gets or Sets The Levels Within 
        /// The Ai Players Hands
        /// </summary>
        public int[] AILevel { get { return this.levels; } 
                               set { this.levels=value; } 
                              } 



        // =====================================
        //Funtions -----------------------------
        // =====================================


        /// <summary>
        /// Sending In Current Direction, AI 
        /// Player & Top Card on Discard Pil will
        /// Return AI Choice For Hand Index
        /// </summary>
        /// <param name="currDir">Rules.direction - none, up, down </param>
        /// <param name="currAI">Integer - Current AI Player</param>
        /// <param name="topPile">Card - Top Of Discard Pile</param>
        /// <returns></returns>
        public int AIDecideLogic(direction currDir, int currAI, Card topPile) {
            int choice = -1; //<--No Choice = -1

            switch(currDir) {
                case direction.none: //<--No Direction
                    for(int i=0; i<this.cardsPerHand; i++) {
                        if(this.hands[currAI, i]!=null && this.hands[currAI, i].value>topPile.value 
                            || this.hands[currAI, i].value<topPile.value)
                            choice=i;
                    }
                    break;
                case direction.up: //<--Up Direction
                    for(int i=0; i<this.cardsPerHand; i++) {
                        if(this.hands[currAI, i]!=null && this.hands[currAI, i].value>topPile.value)
                            choice=i;
                    }
                    break;
                case direction.down: //<--Up Direction
                    for(int i=0; i<this.cardsPerHand; i++) {
                        if(this.hands[currAI, i]!=null && this.hands[currAI, i].value<topPile.value)
                            choice=i;
                    }
                    break;
            } 

            return choice;
                        
        } //end function



        /// <summary>
        /// Looks At All AI Players
        /// To See If Any Players Have Enough
        /// To Win Game.
        /// </summary>
        /// <returns>True or False - Win or No</returns>
        public bool CheckAIWinner() {
            int counter = 0;
            bool answer = false;
            for(int i=0; i<this.numberOfPlayers; i++) {
                for(int y=0; y<this.cardsPerHand; y++) {
                    if(this.hands[i, y]!=null) {
                        counter++;
                    }
                    if(y==(this.cardsPerHand-1) && counter==0) {
                        answer=true;
                    } 
                }
                counter=0;
            }
            
            return answer;
        
        } //end function



    } //EOC

} //EON
