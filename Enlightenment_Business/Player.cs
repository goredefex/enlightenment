﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardLibrary;

namespace Enlightenment_Business {
    public class Player : Rules {
    
        private int level = 1;
        public Card[] hand;
               

        // =====================================
        //Constructs ---------------------------
        // =====================================

        //Takes A Size Of Hand
        public Player(int handSize) {
            this.hand = new Card[handSize];
        
        } //end construct

        // =====================================


        
        // =====================================
        //Fields -------------------------------
        // =====================================
        
        /// <summary>
        /// Gets The Current Level
        /// </summary>
        /// <returns>Integer - Returns Current Level</returns>
        public int Level { get {return this.level; }
                           set { this.level=value; } } //end function



        /// <summary>
        /// Search Cards In Hand For The
        /// Ability To Lay A Card Down, Given 
        /// The Card Sent In
        /// </summary>
        /// <param name="currDir">direction - Direction of the gameplay</param>
        /// <param name="topPile">Card - Card sitting on the discard pile</param>
        /// <returns>True or False - IF there are any moves for the user</returns>
        public bool CheckHandForMoves(direction currDir, Card topPile) {
        
            bool moves = false;

            switch(currDir) {
                case direction.none: //<--No Direction
                    moves = true;
                    break;
                case direction.up: //<--Up Direction
                    for(int i=0; i<this.hand.Length; i++) {
                        if(this.hand[i]!=null && this.hand[i].value>topPile.value) {
                            moves=true;
                            i=this.hand.Length;
                        }
                    }
                    break;
                case direction.down: //<--Up Direction
                    for(int i=0; i<this.hand.Length; i++) {
                        if(this.hand[i]!=null && this.hand[i].value<topPile.value) {
                            moves=true;
                            i=this.hand.Length;
                        }
                    }
                    break;
            } 
           
            return moves;
        
        } //end function

        // =====================================

    
    } //EOC

} //EON
