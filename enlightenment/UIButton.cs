﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Enlightenment_Presentation {
    public class UIButton : RectangleView {


        //Button Sizing
        private const int buttonWidth = 80;
        private const int buttonHeight = 25;
        private const int txtLabelWidth = 65;
        private const int txtLabelHeight = 20;
        private const int labelPaddingTop = 3;
        private const int labelPaddingLeft = 15;

        //Button Control Vars
        private string btnText = String.Empty;
        private Label btnTextLabel;


        // =====================================
        //Constructs ---------------------------
        // =====================================

        public UIButton(Point loc, Panel panel, string newText) {
            this.setX(loc.X);
            this.setY(loc.Y);
            this.setWidth(buttonWidth);
            this.setHeight(buttonHeight);
            this.btnText = newText;
            this.ChangeColor(Color.Black);
            this.MakeAndSetDecals(panel);
        
        } //end construct

        public UIButton(int newX, int newY, Panel panel, string newText) {
            this.setX(newX);
            this.setY(newY);
            this.setWidth(buttonWidth);
            this.setHeight(buttonHeight);
            this.btnText = newText;
            this.ChangeColor(Color.Black);
            this.MakeAndSetDecals(panel);
                    
        } //end construct

        // =====================================


        // =====================================
        //Functions ----------------------------
        // =====================================

        public void DrawButton(PaintEventArgs e) {
            this.MakeRectangle(e);
        
        } //end event

        public void MakeAndSetDecals(Panel panel) {
        
            this.btnTextLabel = new Label();
            this.btnTextLabel.BackColor = Color.Black;
            this.btnTextLabel.ForeColor = Color.White;
            this.btnTextLabel.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Bold);;
            this.btnTextLabel.Text = this.btnText;
            this.btnTextLabel.SetBounds((this.getX()+labelPaddingLeft), (this.getY()+labelPaddingTop), txtLabelWidth, txtLabelHeight);
            panel.Controls.Add(this.btnTextLabel);
        
        } //end function

        // =====================================


    } //EOC

} //EON
