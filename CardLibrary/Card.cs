﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardLibrary {
    public class Card {
       
        //Card Field
        public int value { get; private set; }
        public int secondaryValue;
        public int suit { get; private set; }
        public bool isTrump { get; set; }

        //Constructs --------------------------
        public Card() {} // <--Enables Null Obj Ref

        //Takes Card Value & Suit
        public Card(int cardVal, int suitType) {
            this.value = this.GetCardPotential(cardVal);
            this.suit = suitType;
            this.isTrump = false;
            
        } //end constructor

        // ====================================


        //Functions ---------------------------

        /// <summary>
        /// Searches Card Value To 
        /// Ensure Double Values Are Met
        /// </summary>
        /// <param name="currValue">Integer - Value</param>
        /// <returns>Integer - Value Is Filtered & Returned</returns>
        private int GetCardPotential(int currValue) {
            if(currValue == (int)Deck.cardValue.ace) {
                this.secondaryValue = (int)Deck.cardValue.Ace;
            } else if(currValue == (int)Deck.cardValue.Ace) { 
                this.secondaryValue = (int)Deck.cardValue.ace;
            }
            
            return currValue;
        
        } //end function


        /// <summary>
        /// Do No Use Secondary Value In This Card
        /// </summary>
        public void TurnOffSecondary() {
            this.secondaryValue = 0;
            
        } //end function



    } //EOC

} //EON
